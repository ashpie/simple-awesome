---------------------------
-- Default awesome theme --
---------------------------

local awful = require("awful")
local naughty = require("naughty")
local gears = require("gears")

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gfs = require("gears.filesystem")
local themes_path = gfs.get_themes_dir()
local simple_awesome_path = awful.util.getdir("config")

local theme = {}

theme.font          = "sans 12"

theme.bg_normal     = "#222222"
theme.bg_focus      = "#282828"
theme.bg_urgent     = "#bf221c"
theme.bg_minimize   = "#111111"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#aaaaaa"
theme.fg_focus      = "#ffffff"
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#ffffff"

theme.useless_gap   = dpi(8)
theme.border_width  = dpi(0)
theme.border_normal = "#555555"
theme.border_focus  = "#555555"
theme.border_marked = "#91231c"

-- Tasklist
theme.tasklist_bg_focus = theme.bg_minimize
theme.tasklist_bg_minimize = theme.bg_normal

-- Notifications
naughty.config.padding = dpi(8)
naughty.config.defaults.position = "top_right"
naughty.config.defaults.margin = dpi(16)
naughty.config.defaults.border_width = dpi(0)
naughty.config.presets.critical.bg = "#e82922"
theme.notification_shape = function(cr, width, height)
    return gears.shape.rounded_rect(cr, width, height, 5)
end
theme.notification_margin = dpi(16)
theme.notification_width = dpi(512)
theme.notification_border_color = theme.border_focus
theme.notification_border_width = dpi(0)
theme.notification_bg = theme.bg_focus
theme.notification_fg = theme.fg_focus

-- Hotkeys (awesome help menu)
theme.hotkeys_border_color = theme.border_normal

-- System resources widget
theme.system_resources_widget_bar_bg = theme.bg_focus
theme.system_resources_widget_bar_color = "#ffffff"
theme.system_resources_widget_border_color = theme.fg_normal
theme.system_resources_widget_border_width = 1
theme.system_resources_widget_bar_width = dpi(8)
theme.system_resources_widget_bar_margin = dpi(2)
theme.system_resources_widget_bar_shape = function(cr, width, height)
    return gears.shape.rounded_rect (cr, width, height, 2)
end
theme.system_resources_widget_battery_margin = theme.system_resources_widget_bar_width

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

-- Generate taglist squares:
local taglist_square_size = dpi(8)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = themes_path.."default/submenu.png"
theme.menu_height = dpi(32)
theme.menu_width  = dpi(256)

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal = simple_awesome_path .. "/assets/icons/x.svg"
theme.titlebar_close_button_focus = theme.titlebar_close_button_normal
theme.titlebar_close_button_hover_bg_color = '#800'

theme.titlebar_maximized_button_normal_inactive = simple_awesome_path .. "/assets/icons/maximize.svg"
theme.titlebar_maximized_button_focus_inactive = theme.titlebar_maximized_button_normal_inactive
theme.titlebar_maximized_button_normal_active = simple_awesome_path .. "/assets/icons/unmaximize.svg"
theme.titlebar_maximized_button_focus_active = theme.titlebar_maximized_button_normal_active
theme.titlebar_maximized_button_hover_bg_color = '#555'

theme.titlebar_minimize_button_normal = simple_awesome_path .. "/assets/icons/minus.svg"
theme.titlebar_minimize_button_focus = theme.titlebar_minimize_button_normal
theme.titlebar_minimize_button_hover_bg_color = '#555'

theme.titlebar_ontop_button_normal_inactive = themes_path.."default/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive = theme.titlebar_ontop_button_normal_inactive
theme.titlebar_ontop_button_normal_active = themes_path.."default/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active = theme.titlebar_ontop_button_normal_active
theme.titlebar_ontop_button_hover_bg_color = '#555'

theme.titlebar_floating_button_normal_inactive = themes_path.."default/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive = theme.titlebar_floating_button_normal_inactive
theme.titlebar_floating_button_normal_active = themes_path.."default/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active = theme.titlebar_floating_button_normal_active
theme.titlebar_floating_button_hover_bg_color = '#555'


theme.wallpaper = simple_awesome_path.."/assets/art/default_wallpaper.png"

-- You can use your own layout icons like this:
theme.layout_fairh = themes_path.."default/layouts/fairhw.png"
theme.layout_fairv = themes_path.."default/layouts/fairvw.png"
theme.layout_floating  = themes_path.."default/layouts/floatingw.png"
theme.layout_magnifier = themes_path.."default/layouts/magnifierw.png"
theme.layout_max = themes_path.."default/layouts/maxw.png"
theme.layout_fullscreen = themes_path.."default/layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path.."default/layouts/tilebottomw.png"
theme.layout_tileleft   = themes_path.."default/layouts/tileleftw.png"
theme.layout_tile = themes_path.."default/layouts/tilew.png"
theme.layout_tiletop = themes_path.."default/layouts/tiletopw.png"
theme.layout_spiral  = themes_path.."default/layouts/spiralw.png"
theme.layout_dwindle = themes_path.."default/layouts/dwindlew.png"
theme.layout_cornernw = themes_path.."default/layouts/cornernww.png"
theme.layout_cornerne = themes_path.."default/layouts/cornernew.png"
theme.layout_cornersw = themes_path.."default/layouts/cornersww.png"
theme.layout_cornerse = themes_path.."default/layouts/cornersew.png"

-- Generate Awesome icon:
theme.start_menu_icon = simple_awesome_path .. "/assets/icons/material-tux-dark-keen.svg"
theme.awesome_icon = theme_assets.awesome_icon(
    theme.menu_height, theme.bg_focus, theme.fg_focus
)

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

-- Volume OSD
theme.volume_osd_width = dpi(48)
theme.volume_osd_bar_height = dpi(196)
theme.volume_osd_padding = dpi(8)
theme.volume_osd_shape = function(cr, width, height)
    return gears.shape.rounded_rect(cr, width, height, 5)
end
theme.volume_osd_position = function(screen_width, screen_height, osd_width, osd_height)
    return {
        dpi(8),
        theme.menu_height + dpi(8)
    }
end

theme.volume_osd_bg = theme.bg_normal
theme.volume_osd_border_color = theme.border_normal
theme.volume_osd_border_width = dpi(0)
theme.volume_osd_progress_bg = theme.bg_minimize
theme.volume_osd_progress_color = "#ffffff"
theme.volume_osd_progress_border_color = theme.volume_osd_border_color
theme.volume_osd_progress_border_width = dpi(0)
theme.volume_osd_image_color = "#ffffff"
theme.volume_osd_icon_0 = simple_awesome_path .. "/assets/icons/volume-x.svg"
theme.volume_osd_icon_1 = simple_awesome_path .. "/assets/icons/volume.svg"
theme.volume_osd_icon_2 = simple_awesome_path .. "/assets/icons/volume-1.svg"
theme.volume_osd_icon_3 = simple_awesome_path .. "/assets/icons/volume-2.svg"


-- Power Menu
theme.power_menu_icon_color = "#ffffff"

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
