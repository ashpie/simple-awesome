# Simple Awesome

As simple as it sounds
(not yet though)

## Dependencies

By default, Simple Awesome uses

- `rofi` - launch bar / window switcher
- `flameshot` - screenshot utility
- `alsa-utils` - amixer command provider (media keys volume control)

## Optional dependencies

- `playerctl` - enables global media control. If not present on your system, only Spotify will work.
- `network-manager-applet` (`nm-applet`) - network control "widget" **for `NetworkManager` only**. If present on your system, we will automatically run it.
- `i3lock-color` - lock your screen in a simple and elegant way

## To do

- Add a sound volume control widget
- Add a media control widget
- Manage workspaces
- Improve "leave menu"'s design
- Modify "start menu", make everything work
- Make "start menu" and "leave menu" not sticky (they currently stay open and focus even when clicking outside)
- Replace the os menu with a custom widget
- Add backlight media keys to xbacklight command functionnality
- Add icon to the battery widget
- Make mouse auto focus configurable
- Make desktop entries menu
- Alt tab tab tab to switch

## Arisu personal todo

- Add aliases to rofi https://github.com/davatorium/rofi/issues/97
