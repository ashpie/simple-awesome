-- Default configuration file
-- This file will be overridden on update.
-- Copy to `~/.config/awesome/config.lua` to avoid losing your changes.
local awful = require("awful")
local naughty = require("naughty")


local config = {}


--
-- Awesome WM
--
config.awesome = {
    -- Enable window autofocus on mouse hover
    autofocus = true,

    -- Enable hotkeys help widget for VIM and other apps
    -- when client with a matching name is opened:
    hotkeys_popup = true,

    -- Application launcher
    application_launcher_check = "rofi -version",
    application_launcher = "rofi -show drun -modi drun -show-icons -config " .. awful.util.getdir("config") .. "/assets/rofi/themes/paper-float.rasi",
    application_launcher_cmd = "rofi -show run -config " .. awful.util.getdir("config") .. "/assets/rofi/themes/paper-float.rasi",

    -- Screen locker
    lock_command = "i3lock --blur=5 -f -k --timecolor=ffffffff --datecolor=ffffffff --indicator --ringcolor=aaaaaaaa",
    -- lock_command = "xscreensaver-command -lock",

    -- Terminal
    terminal = "terminator",

    -- Window tiling
    tiling = {
        -- Available window layouts - order matters
        layouts = {
            awful.layout.suit.floating,
            awful.layout.suit.tile,
            awful.layout.suit.tile.left,
            awful.layout.suit.tile.bottom,
            awful.layout.suit.tile.top,
            -- awful.layout.suit.fair,
            -- awful.layout.suit.fair.horizontal,
            -- awful.layout.suit.spiral,
            -- awful.layout.suit.spiral.dwindle,
            -- awful.layout.suit.max,
            -- awful.layout.suit.max.fullscreen,
            -- awful.layout.suit.magnifier,
            -- awful.layout.suit.corner.nw,
            -- awful.layout.suit.corner.ne,
            -- awful.layout.suit.corner.sw,
            -- awful.layout.suit.corner.se,
        },
    },
}

-- Default text editor
config.awesome.editor = os.getenv("EDITOR") or "nano"
-- config.awesome.editor = "vi"
-- config.awesome.editor = "code"

-- Default text editor command
config.awesome.editor_cmd = config.awesome.terminal .. " -e " .. config.awesome.editor


--
-- Keys / Hotkeys / Shortcuts
--
-- To disable a shortcut, simply remove it (tip: comment it out with a leading "--")
--
config.keys = {
    -- Main modifier key for meta hotkeys like moving windows or locking the session
    -- Usually, Mod4 is the key with a logo between Control and Alt.
    modkey = "Mod4",
}

-- Hotkeys globally available (through the entire OS, regardless of what's focused)
config.keys.global_keys = {
    -- Application launcher
    launcher = { { config.keys.modkey }, "space", },
    launcher_cmd = { { "Ctrl", config.keys.modkey }, "space", },

    -- Screen locker
    lock_screen = { { config.keys.modkey }, "l" },


    --
    -- Media and volume controls
    -- See media_control.lua
    --
    media_control = {
        -- Raise volume
        raiseVolume = { {}, "XF86AudioRaiseVolume" },

        -- Lower volume
        lowerVolume = { {}, "XF86AudioLowerVolume" },

        -- Toggle mute
        toggleMute = { {}, "XF86AudioMute" },

        -- Toggle Play / Pause
        playPause = { {}, "XF86AudioPlay" },

        -- Next track
        nextTrack = { {}, "XF86AudioNext" },

        -- Previous track
        previousTrack = { {}, "XF86AudioPrev" },

        -- Brightness up
        brightnessUp = { {}, "XF86MonBrightnessUp" },

        -- Brightness down
        brightnessDown = { {}, "XF86MonBrightnessDown" },
    },

    -- Keyboard layout
    keyboard_layout = {
        -- Switch between layouts
        switch = { { "Mod1" }, "Shift_L" },

        -- Switch between layouts (alternative)
        switch_alt = { { "Shift" }, "Alt_L" },
    }
}

-- Custom global hotkeys
config.keys.custom_keys = {
    -- Example:
    -- {
    --     hotkey = {
    --         { config.keys.modkey, },
    --         "t"
    --     },
    --     action = function()
    --         awful.spawn('terminator')
    --     end,
    --     identity = { description = "open terminator", group = "utilities" }
    -- }
};


--
-- Audio
--
config.audio = {
	card = '-D pulse',
	device = 'Master',
};


--
-- Widgets
--
config.widgets = {
    -- Keyboard layout widget
    keyboard_layout = {
        enabled = true,
        layouts = { "fr", "us" },
        layout_switch_command = "setxkbmap",
    },

    -- Enable/disable the system resources monitoring widget
    system_resources = {
        bars = {
            enabled = true,
            screens = { 1, 2 },
            refresh_interval = 1.5, -- In seconds
        },
        battery = {
            enabled = true,
            screens = { 1, 2 },
            refresh_interval = 30, -- In seconds
            precision = 0, -- How many decimals
        },
    },
}


--
-- Auto start custom applications
--
config.autostart = {
    -- Network manager widget
    {
        "nm-applet --help", -- Verification command that will be checked before spawning the next command
        "nm-applet", -- The app
    },

    -- Screenshot utility
    "flameshot",
}


-- Theme
-- Change to gears.filesystem.get_themes_dir() .. "default/theme.lua" to use default awesome theme
config.theme = awful.util.getdir("config") .. "/theme.lua"

-- Screenshot utility
config.screenshot_utility_command = "flameshot gui"

-- Run menu
config.windowSwitcher = "rofi -show window -config " .. awful.util.getdir("config") .. "/assets/rofi/themes/paper-float.rasi"

-- Media control
config.volume_osd_timeout = 2


-- Awesome tweaks
naughty.config.defaults.timeout = 15


return config
