local awful = require("awful")
local gears = require("gears")

local global_keys = {}

function make(hotkey, action, identity)
    if hotkey == nil then
        return nil
    end

    local modifiers = hotkey[1]
    local key = hotkey[2]
    return awful.key(modifiers, key, action, identity)
end


function registerKeys(...)
    global_keys = gears.table.join(global_keys, ...)
end

function getKeys()
    return global_keys
end

return {
    make = make,
    registerKeys = registerKeys,
    getKeys = getKeys,
}