--
-- DEBUG
--
local inspect = require("simple/debug/inspect")

--
-- Simple Awesome configuration
--
local config = require("config")


--
-- Hotkeys
--
local hotkey = require("simple/core/hotkey")


--
-- Misc components
--
local titlebar = require("simple/titlebar")


--
-- Awesome WM
--
local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local naughty = require("naughty")
local wibox = require("wibox")

-- Widgets and layout components
local hotkeys_popup = require("awful.hotkeys_popup")

-- Window autofocus
if config.awesome.autofocus then
    require("awful.autofocus")
end

-- Hotkeys popup
if hotkeys_popup then
    require("awful.hotkeys_popup.keys")
end

-- Application launcher
local menubar = require("menubar")
menubar.utils.terminal = config.awesome.terminal
function showAppLauncher(commandOnly)
    if os.execute(config.awesome.application_launcher_check) then
        if commandOnly then
            awful.spawn(config.awesome.application_launcher_cmd)
        else
            awful.spawn(config.awesome.application_launcher)
        end
    else
        menubar.show()
    end
end

hotkey.registerKeys(hotkey.make(config.keys.global_keys.launcher, function() showAppLauncher(false) end,
    { description = "show the application launcher", group = "launcher" }))
hotkey.registerKeys(hotkey.make(config.keys.global_keys.launcher_cmd, function() showAppLauncher(true) end,
    { description = "show the application launcher (command line)", group = "launcher" }))


--
-- Error handling
--

-- Startup errors
if awesome.startup_errors then
    naughty.notify({
        preset = naughty.config.presets.critical,
        title = "Something went wrong while booting Awesome",
        text = awesome.startup_errors
    })
end

-- Runtime errors handling routine
do
    local in_error = false
    awesome.connect_signal("debug::error", function(err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({
            preset = naughty.config.presets.critical,
            title = "An error happened during Awesome runtime",
            text = tostring(err)
        })
        in_error = false
    end)
end


--
-- Theme
--
beautiful.init(config.theme)


--
-- Window tiling
--
local tiling = require("simple/tiling")

-- Tiling layouts
awful.layout.layouts = config.awesome.tiling.layouts


--
-- Media control keys and widget
--
local media_control = require("simple/media_control")
hotkey.registerKeys(media_control.getKeys(config.keys.global_keys.media_control))


--
-- Widgets
--

-- System resources
local system_resources_widget = require("simple/widgets/system_resources")

-- Keyboard map indicator and switcher
local keyboard_layout = require("simple/widgets/keyboard_layout")
local keyboard_layout_widget
if config.widgets.keyboard_layout.enabled then
    keyboard_layout_widget = keyboard_layout.widget
end
hotkey.registerKeys(keyboard_layout.getKeys(config.keys.global_keys.keyboard_layout))


--
-- Auto start
--
for _, app in ipairs(config.autostart) do
    local type = type(app)
    if type == "table" then -- Has a verification command that checks if it exists on the system
        if os.execute(app[1]) == true then
            awful.spawn(app[2])
        end
    elseif type == "string" then -- Simple command
        awful.spawn(app)
    end
end


--
-- Screen locker
--
function lockScreen()
    awful.spawn(config.awesome.lock_command)
end

hotkey.registerKeys(hotkey.make(config.keys.global_keys.lock_screen, lockScreen,
    { description = "lock your screen", group = "awesome" }))


--
-- Launcher (start menu)
--
local launcher = awful.widget.button({
    image = beautiful.start_menu_icon,
})
launcher:buttons(gears.table.join(launcher:buttons(),
    awful.button({}, 1, nil, function() showAppLauncher(false) end)))

shutdown_icon = gears.color.recolor_image(awful.util.getdir("config") .. "/assets/icons/power.svg", beautiful.power_menu_icon_color)
restart_icon = gears.color.recolor_image(awful.util.getdir("config") .. "/assets/icons/refresh-cw.svg", beautiful.power_menu_icon_color)
logout_icon = gears.color.recolor_image(awful.util.getdir("config") .. "/assets/icons/log-out.svg", beautiful.power_menu_icon_color)
lock_icon = gears.color.recolor_image(awful.util.getdir("config") .. "/assets/icons/lock.svg", beautiful.power_menu_icon_color)

os_menu = awful.widget.launcher({
    image = beautiful.awesome_icon,
    menu = awful.menu({
        items = {
            { "Hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
            { "Manual", config.awesome.terminal .. " -e man awesome" },
            { "Edit config", config.awesome.editor_cmd .. " " .. awesome.conffile },
            { "Open terminal", config.awesome.terminal },
            { "Lock", lockScreen, lock_icon },
            { "Logout", function() awesome.quit() end, logout_icon },
            { "Restart", function() os.execute('shutdown -r now') end, restart_icon },
            { "Shutdown", function() os.execute('shutdown now') end, shutdown_icon },
        }
    })
})


-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock()

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(awful.button({}, 1, function(t) t:view_only() end),
    awful.button({ config.keys.modkey }, 1, function(t)
        if client.focus then
            client.focus:move_to_tag(t)
        end
    end),
    awful.button({}, 3, awful.tag.viewtoggle),
    awful.button({ config.keys.modkey }, 3, function(t)
        if client.focus then
            client.focus:toggle_tag(t)
        end
    end),
    awful.button({}, 4, function(t) awful.tag.viewnext(t.screen) end),
    awful.button({}, 5, function(t) awful.tag.viewprev(t.screen) end))

local tasklist_buttons = gears.table.join(awful.button({}, 1, function(c)
    if c == client.focus then
        c.minimized = true
    else
        c:emit_signal("request::activate",
            "tasklist",
            { raise = true })
    end
end),
    awful.button({}, 3, function()
        awful.menu.client_list({ theme = { width = 250 } })
    end),
    awful.button({}, 4, function()
        awful.client.focus.byidx(1)
    end),
    awful.button({}, 5, function()
        awful.client.focus.byidx(-1)
    end))

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
    awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(awful.button({}, 1, function() awful.layout.inc(1) end),
        awful.button({}, 3, function() awful.layout.inc(-1) end),
        awful.button({}, 4, function() awful.layout.inc(1) end),
        awful.button({}, 5, function() awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen = s,
        filter = awful.widget.taglist.filter.all,
        buttons = taglist_buttons
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen = s,
        filter = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons,
        widget_template = {
            {
                {
                    {
                        {
                            id = 'icon_role',
                            widget = wibox.widget.imagebox,
                        },
                        margins = 8,
                        widget = wibox.container.margin,
                    },
                    {
                        id = 'text_role',
                        widget = wibox.widget.textbox,
                    },
                    layout = wibox.layout.fixed.horizontal,
                },
                left = 0,
                right = 8,
                widget = wibox.container.margin
            },
            id = 'background_role',
            widget = wibox.container.background,
        },
        layout = {
            max_widget_size = 256,
            layout = wibox.layout.flex.horizontal
        }
    }

    -- Create the wibox
    s.mywibox = awful.wibar({
        position = "top",
        screen = s,
        height = 32
    })

    function matchesScreen(screens)
        for _, allowedScreen in ipairs(screens) do
            if allowedScreen == s.index then
                return true
            end
        end
        return false
    end

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        {
            -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            launcher,
            s.mytaglist,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        {
            -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            matchesScreen(config.widgets.system_resources.bars.screens) and system_resources_widget.bars or nil,
            matchesScreen(config.widgets.system_resources.battery.screens) and system_resources_widget.battery or nil,
            keyboard_layout_widget,
            wibox.widget.systray(),
            mytextclock,
            s.mylayoutbox,
            os_menu,
        },
    }
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(awful.button({}, 3, function() mymainmenu:toggle() end),
    awful.button({}, 4, awful.tag.viewnext),
    awful.button({}, 5, awful.tag.viewprev)))
-- }}}

-- {{{ Key bindings

hotkey.registerKeys(
	awful.key({ config.keys.modkey }, "p",
    	function()
			local screen = awful.screen.focused()
			screen.mywibox.visible = not screen.mywibox.visible
    	end,
	{ description = "hide status bar", group = "screen" }))

hotkey.registerKeys(
    awful.key({ config.keys.modkey, }, "s", hotkeys_popup.show_help,
        { description = "show help", group = "awesome" }),

    awful.key({ config.keys.modkey, }, "Escape", awful.tag.history.restore,
        { description = "go back", group = "tag" }),

    awful.key({ config.keys.modkey, }, "j",
        function()
            awful.client.focus.byidx(1)
        end,
        { description = "focus next by index", group = "client" }),
    awful.key({ config.keys.modkey, }, "k",
        function()
            awful.client.focus.byidx(-1)
        end,
        { description = "focus previous by index", group = "client" }),
    awful.key({ config.keys.modkey, }, "w", function() mymainmenu:show() end,
        { description = "show main menu", group = "awesome" }),

    -- Layout manipulation
    awful.key({ config.keys.modkey, "Shift" }, "j", function() awful.client.swap.byidx(1) end,
        { description = "swap with next client by index", group = "client" }),
    awful.key({ config.keys.modkey, "Shift" }, "k", function() awful.client.swap.byidx(-1) end,
        { description = "swap with previous client by index", group = "client" }),
    awful.key({ config.keys.modkey, "Control" }, "j", function() awful.screen.focus_relative(1) end,
        { description = "focus the next screen", group = "screen" }),
    awful.key({ config.keys.modkey, "Control" }, "k", function() awful.screen.focus_relative(-1) end,
        { description = "focus the previous screen", group = "screen" }),
    awful.key({ config.keys.modkey, }, "u", awful.client.urgent.jumpto,
        { description = "jump to urgent client", group = "client" }),
    awful.key({ config.keys.modkey, }, "Tab",
        function()
            if config.windowSwitcher == "" then
                awful.client.focus.history.previous()
                if client.focus then
                    client.focus:raise()
                end
            else
                os.execute(config.windowSwitcher)
            end
        end,
        { description = "switch window", group = "client" }),

    -- Standard program
    awful.key({ config.keys.modkey, }, "Return", function() awful.spawn(config.awesome.terminal) end,
        { description = "open a terminal", group = "launcher" }),
    awful.key({ config.keys.modkey, "Control" }, "r", awesome.restart,
        { description = "reload awesome", group = "awesome" }),
    awful.key({ config.keys.modkey, "Shift" }, "q", awesome.quit,
        { description = "quit awesome", group = "awesome" }),

    --    awful.key({ config.keys.modkey, }, "l", function() awful.tag.incmwfact(0.05) end,
    --        { description = "increase master width factor", group = "layout" }),
    --    awful.key({ config.keys.modkey, }, "h", function() awful.tag.incmwfact(-0.05) end,
    --        { description = "decrease master width factor", group = "layout" }),
    awful.key({ config.keys.modkey, "Shift" }, "h", function() awful.tag.incnmaster(1, nil, true) end,
        { description = "increase the number of master clients", group = "layout" }),
    awful.key({ config.keys.modkey, "Shift" }, "l", function() awful.tag.incnmaster(-1, nil, true) end,
        { description = "decrease the number of master clients", group = "layout" }),
    awful.key({ config.keys.modkey, "Control" }, "h", function() awful.tag.incncol(1, nil, true) end,
        { description = "increase the number of columns", group = "layout" }),
    awful.key({ config.keys.modkey, "Control" }, "l", function() awful.tag.incncol(-1, nil, true) end,
        { description = "decrease the number of columns", group = "layout" }),
    awful.key({ config.keys.modkey, }, "v", function() awful.layout.inc(1) end,
        { description = "select next", group = "layout" }),
    awful.key({ config.keys.modkey }, "c", function() awful.layout.inc(-1) end,
        { description = "select previous", group = "layout" }),

    awful.key({ config.keys.modkey, "Control" }, "n",
        function()
            local c = awful.client.restore()
            -- Focus restored client
            if c then
                c:emit_signal("request::activate", "key.unminimize", { raise = true })
            end
        end,
        { description = "restore minimized", group = "client" }),

    -- Prompt
    awful.key({ config.keys.modkey }, "r", function() os.execute(config.runMenu) end,
        { description = "show the run menu", group = "launcher" }),

    awful.key({ config.keys.modkey }, "x",
        function()
            awful.prompt.run {
                prompt = "Run Lua code: ",
                textbox = awful.screen.focused().mypromptbox.widget,
                exe_callback = awful.util.eval,
                history_path = awful.util.get_cache_dir() .. "/history_eval"
            }
        end,
        { description = "lua execute prompt", group = "awesome" }),
    -- Menubar
    awful.key({ config.keys.modkey }, "space", function() os.execute(config.runMenu) end,
        { description = "show the run menu", group = "launcher" }),
    -- Flameshot
    awful.key({}, "Print", function() os.execute(config.screenshot_utility_command) end,
        { description = "Take a screenshot using the screenshot utility", group = "utilities" })
)

for key in gears.table.iterate(config.keys.custom_keys, function() return true end) do
    hotkey.registerKeys(
        hotkey.make(key.hotkey, key.action, key.identity)
    )
end

clientkeys = gears.table.join(-- tiling
    awful.key({ config.keys.modkey, }, "Right",
        tiling.key.tileRight,
        { description = "Tile right", group = "tiling" }),
    awful.key({ config.keys.modkey, }, "Left",
        tiling.key.tileLeft,
        { description = "Tile left", group = "tiling" }),
    awful.key({ config.keys.modkey, "Control" }, "Right",
        tiling.key.screenRight,
        { description = "Screen right", group = "tiling" }),
    awful.key({ config.keys.modkey, "Control" }, "Left",
        tiling.key.screenLeft,
        { description = "Screen left", group = "tiling" }),
    awful.key({ config.keys.modkey, }, "Up",
        tiling.key.toggleMaximized,
        { description = "Toggle maximized", group = "tiling" }),
    awful.key({ config.keys.modkey, }, "Down",
        tiling.key.minimize,
        { description = "Lower", group = "tiling" }),

    awful.key({ config.keys.modkey, }, "f",
        function(c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        { description = "toggle fullscreen", group = "client" }),
    awful.key({ config.keys.modkey, "Shift" }, "c", function(c) c:kill() end,
        { description = "close", group = "client" }),
    awful.key({ config.keys.modkey, "Control" }, "space", awful.client.floating.toggle,
        { description = "toggle floating", group = "client" }),
    awful.key({ config.keys.modkey, "Control" }, "Return", function(c) c:swap(awful.client.getmaster()) end,
        { description = "move to master", group = "client" }),
    awful.key({ config.keys.modkey, }, "o", function(c) c:move_to_screen() end,
        { description = "move to screen", group = "client" }),
    awful.key({ config.keys.modkey, }, "t", function(c) c.ontop = not c.ontop end,
        { description = "toggle keep on top", group = "client" }),
    awful.key({ config.keys.modkey, }, "n",
        function(c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end,
        { description = "minimize", group = "client" }),
    awful.key({ config.keys.modkey, }, "m",
        function(c)
            c.maximized = not c.maximized
            c:raise()
        end,
        { description = "(un)maximize", group = "client" }),
    awful.key({ config.keys.modkey, "Control" }, "m",
        function(c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end,
        { description = "(un)maximize vertically", group = "client" }),
    awful.key({ config.keys.modkey, "Shift" }, "m",
        function(c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end,
        { description = "(un)maximize horizontally", group = "client" }))

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    hotkey.registerKeys(-- View tag only.
        awful.key({ config.keys.modkey }, "#" .. i + 9,
            function()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    tag:view_only()
                end
            end,
            { description = "view tag #" .. i, group = "tag" }),
        -- Toggle tag display.
        awful.key({ config.keys.modkey, "Control" }, "#" .. i + 9,
            function()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    awful.tag.viewtoggle(tag)
                end
            end,
            { description = "toggle tag #" .. i, group = "tag" }),
        -- Move client to tag.
        awful.key({ config.keys.modkey, "Shift" }, "#" .. i + 9,
            function()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:move_to_tag(tag)
                    end
                end
            end,
            { description = "move focused client to tag #" .. i, group = "tag" }),
        -- Toggle tag on focused client.
        awful.key({ config.keys.modkey, "Control", "Shift" }, "#" .. i + 9,
            function()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:toggle_tag(tag)
                    end
                end
            end,
            { description = "toggle focused client on tag #" .. i, group = "tag" }))
end

clientbuttons = gears.table.join(awful.button({}, 1, function(c)
    c:emit_signal("request::activate", "mouse_click", { raise = true })
end),
    awful.button({ config.keys.modkey }, 1, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.move(c)
    end),
    awful.button({ config.keys.modkey }, 3, function(c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.resize(c)
    end))

-- Set keys
root.keys(hotkey.getKeys())
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    {
        rule = {},
        properties = {
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = awful.client.focus.filter,
            raise = true,
            keys = clientkeys,
            buttons = clientbuttons,
            screen = awful.screen.preferred,
            placement = awful.placement.no_overlap + awful.placement.no_offscreen
        }
    },

    -- Floating clients.
    {
        rule_any = {
            instance = {
                "DTA", -- Firefox addon DownThemAll.
                "copyq", -- Includes session name in class.
                "pinentry",
            },
            class = {
                "Arandr",
                "Blueman-manager",
                "Gpick",
                "Kruler",
                "MessageWin", -- kalarm.
                "Sxiv",
                "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
                "Wpa_gui",
                "veromix",
                "xtightvncviewer"
            },

            -- Note that the name property shown in xprop might be set slightly after creation of the client
            -- and the name shown there might not match defined rules here.
            name = {
                "Event Tester", -- xev.
            },
            role = {
                "AlarmWindow", -- Thunderbird's calendar.
                "ConfigManager", -- Thunderbird's about:config.
                "pop-up",
            },
            type = {
                "dialog",
                "floating",
            }
        },
        properties = { floating = true }
    },

    -- Add titlebars to normal clients and dialogs
    {
        rule_any = {
            type = { "normal" }
        },
        properties = { titlebars_enabled = true }
    },

    -- Set Firefox to always map on the tag named "2" on screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { screen = 1, tag = "2" } },
}
-- }}}

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(awful.button({}, 1, function()
        c:emit_signal("request::activate", "titlebar", { raise = true })
        awful.mouse.client.move(c)
    end),
        awful.button({}, 3, function()
            c:emit_signal("request::activate", "titlebar", { raise = true })
            awful.mouse.client.resize(c)
        end))

    awful.titlebar(c):setup {
        {
            {
                awful.titlebar.widget.iconwidget(c),
                buttons = buttons,
                layout = wibox.layout.fixed.horizontal
            },
            margins = 2,
            widget = wibox.container.margin,
        },
        {
            {
                {
                    -- Title
                    align = "left",
                    widget = awful.titlebar.widget.titlewidget(c)
                },
                buttons = buttons,
                layout = wibox.layout.flex.horizontal
            },
            left = 8,
            right = 8,
            widget = wibox.container.margin,
        },
        {
            titlebar.widget.floatingbutton(c),
            titlebar.widget.ontopbutton(c),
            titlebar.widget.minimizebutton(c),
            titlebar.widget.maximizedbutton(c),
            titlebar.widget.closebutton(c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

-- Enable sloppy focus, so that focus follows mouse.
if config.awesome.autofocus then
    client.connect_signal("mouse::enter", function(c)
        c:emit_signal("request::activate", "mouse_enter", { raise = false })
    end)
end

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}
