local awful = require("awful")

local hotkey = require("simple/core/hotkey")

local config = require("config")


local widget = awful.widget.keyboardlayout()
local layouts = config.widgets.keyboard_layout.layouts

local current_layout_index = 1
function switchLayout()
    current_layout_index = (current_layout_index % #(layouts)) + 1
    os.execute(config.widgets.keyboard_layout.layout_switch_command .. " " .. layouts[current_layout_index])
end

return {
    widget = widget,
    switch = switchLayout,
    getKeys = function(config)
        return hotkey.make(config.switch, switchLayout,
            { description = "switch keyboard layout" }),
        hotkey.make(config.switch_alt, switchLayout,
            { description = "switch keyboard layout (alternative)" })
    end
}