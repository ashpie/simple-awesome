local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')

function button(c, image, hover_bg_color)
    local background = wibox.container.background(image)

    background:connect_signal('mouse::enter', function()
        background.bg = hover_bg_color
    end)

    background:connect_signal('mouse::leave', function()
        background.bg = nil
    end)

    return background
end

function closebutton(c)
    return button(c, awful.titlebar.widget.closebutton(c), beautiful.titlebar_close_button_hover_bg_color)
end

function maximizedbutton(c)
    return button(c, awful.titlebar.widget.maximizedbutton(c), beautiful.titlebar_maximized_button_hover_bg_color)
end

function minimizebutton(c)
    return button(c, awful.titlebar.widget.minimizebutton(c), beautiful.titlebar_minimize_button_hover_bg_color)
end

function ontopbutton(c)
    return button(c, awful.titlebar.widget.ontopbutton(c), beautiful.titlebar_ontop_button_hover_bg_color)
end

function floatingbutton(c)
    return button(c, awful.titlebar.widget.floatingbutton(c), beautiful.titlebar_floating_button_hover_bg_color)
end

return {
    widget = {
        closebutton = closebutton,
        maximizedbutton = maximizedbutton,
        minimizebutton = minimizebutton,
        ontopbutton = ontopbutton,
        floatingbutton = floatingbutton,
    }
}